package com.mx.banorte.services;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mx.banorte.model.Movie;
import com.mx.banorte.util.MovieProducer;

@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MovieResource {

    @Inject
    MovieProducer producer;

    @POST
    @Path("producer")
    public Response send(String body) throws Exception {
        // Crear una instancia de Jsonb para deserializar el JSON en un objeto.
        Jsonb jsonb = JsonbBuilder.create();

        // Deserializar el JSON en un objeto de MiObjeto.
        Movie obj = jsonb.fromJson(body, Movie.class);
        
        try {
            // Asignar informacion de la pelicula a la variable movieInfo.
            String movieInfo = obj.getTitle() + " " + obj.getYear();

            // Utilizar el metodo "send" de la clase producer que agrega un mensaje a la cola de AMQ Broker.
            producer.send(movieInfo);

            // Retorna una respuesta de estatus ok con la informacion de la pelicula.
            return Response.ok(movieInfo, MediaType.APPLICATION_JSON).build();

        } catch (Exception e) {
            // Manejar excepciones
            e.printStackTrace(); // En caso de que se dispare una excepcion, se mostrara en consola.
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error en la solicitud") // Mensaje de error para el cuerpo rest.
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } finally {
            jsonb.close();
        }

    }

}
